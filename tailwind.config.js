const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  mode: 'jit',
  purge: [
    './src/**/*.html',
    './src/**/*.vue',
  ],
  darkMode: 'class', // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        // override grays as in design
        'gray-100': '#f7fafc',
        'gray-200': '#cbd5e0',
        'gray-500': '#718096',
        'gray-600': '#4a5568',
        'gray-800': '#2d3748',

        // greenish
        'primary-light': '#e6fffa',
        'primary': '#81e6d9',
        'primary-dark': '#319795',

        // blueish
        'secondary': '#3182ce',
        'secondary-light': '#ebf4ff',

        'shadow-light': '#d6d6d6',
        'shadow': '#cccccc',
        'shadow-dark': '#707070'

      },
      fontFamily: {
        sans: ['Lato',...defaultTheme.fontFamily.sans],
      },
    },
  },
  variants: {
    extend: {
      fill: ['hover', 'focus'],
    },
  },
  plugins: [
  ],
}
